import sys
import functools

import pandas
from rich.progress import track

from fbaCrank.actions import get_case

@functools.lru_cache
def getTumorScopeCase(caseID: str) -> "Case":
    """ Caching function for fetching TumorScope Case objects """
    return get_case(caseID)

def read_records(state, file, sheet=0, ccol='caseID', dcol='depictionID'):
    #reader = DictReader(open(sys.argv[1]))
    try:
        reader = pandas.read_excel(file, sheet_name=sheet).to_dict('records')
    except: # should catch specific, meh
        reader = pandas.read_csv(file).to_dict('records')

    for line in reader:
        c = line[ccol]
        if dcol is not None:
            d = line[dcol]
            key = f"{c}-{d}"
        else:
            key = c
            d = None
        if key not in state:
            state[key] = {'caseID':c, 'depictionID':d}


def get_verified(caseid):
    case = getTumorScopeCase(caseid)
    v = None
    for i, depiction in enumerate(case.depictions):
        if depiction.status == 'Verified':
            v = depiction.depictionID
            #vs.append((i,depiction.depictionID))
    return v

def get_last(caseid):
    case = getTumorScopeCase(caseid)
    return case.depictions[-1].depictionID

state = {}
read_records(state, sys.argv[1], "address", "caseID_TS", None)

print("caseID,depictionID,verified")
#for k,v in track(list(state.items())):
for k,v in list(state.items()):
    case = v['caseID']
    dpid = v['depictionID']
    try:
        dpid = get_verified(case)
    except TypeError: # no depictions
        print(f"{case},NA,NO DEPICTIONS")
        continue
    if dpid is None:
        dpid = get_last(case)
        print(f"{case},{dpid},no")
    else:
        print(f"{case},{dpid},verified")
