#all these functions set NA to the case dictionary for that particular class of values
def skip_calculations_tf():
    for stat in tf_stats:
        for meas in tumor_features:
            case_dict[stat + '_' + meas + '_' + dict_week] = 'NA'

def skip_calculations_growth():
    for stat in growth_stats:
        case_dict[stat + '_' + 'growth' + '_' + dict_week] = 'NA'

def skip_calculations_gp():
    for stat in gp_stat: 
        for meas in growth_phase:
            case_dict[stat + '_' + meas + '_' + dict_week] = 'NA'

def skip_calculations_nutr():
    for nutr in nutrients:
        for meas in nutr_meas:
            case_dict['low' + '_' + nutr + '_' + meas + '_' + dict_week] = 'NA'

def skip_calculations_mv():
    for stat in mv_stat:
        for meas in mv_meas:
            case_dict[stat + '_' + meas + '_' + dict_week] = 'NA'