from pathlib import Path
import json
from sqlite3 import IntegrityError

from mdptool.dptrack2 import DepictionDB

import sys
db = DepictionDB(sys.argv[1])

from studytool.studytool import parse_mv
from fbaCrank.assets import AssetStore

out = []

from rich.progress import track
for case, s in track(db.getalljobs().items()):
    complete = s.get('COMPLETE', False)
    dpid = s.get('DepictionID')

    if dpid is None:
        print("Error: no depictionid for {case}")
        continue

    mv4 = ("mv4", f"mv-{dpid[10:14]}-mv4")
    mv6 = ("mv6", f"mv-{dpid[10:14]}-mvneg6")
    smoov = ("smoov", f"mv-{dpid[10:14]}-smoov")
    maps = [mv4, mv6, smoov]

    for mvtype, mapID in maps:
        store = AssetStore(case,mapID)
        outrow = {'caseID': case, 'depictionID': dpid, 'type': mvtype, 'mapID': mapID}

        try:
            mvout=store.get('mv_stdout.txt').decode('utf-8')
        except:
            print("Failed to get output for {case}:{mapID}")
            continue

        mvp=parse_mv(mvout)
        if mvp is None:
            print("FAIL ON ",case)
        else:    
            outrow.update(**mvp)

        out.append(outrow)

import pandas
df = pandas.DataFrame(out)
df.to_csv("mvstats.csv", index=False)


