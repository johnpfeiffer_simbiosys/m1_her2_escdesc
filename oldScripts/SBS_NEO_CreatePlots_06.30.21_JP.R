
###############################################################################################################
###############################################################################################################
####################      
####################      MODULE #1: CREATING VOLUME AND LD PLOTS
####################      
###############################################################################################################
###############################################################################################################

library(gridExtra)
library(grid)
library(ggplot2)
library(lattice)
library(pdftools)

getwd()

setwd("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/")

### read in individual simulation results files
TP<-read.csv("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_TP_ITER01_RunSims_Output_06.30.21_JP.csv",header=TRUE)
TP$drug<-"TP"
for (x in 1:nrow(TP)) {
  TP[x,"case_time"]<-paste0(TP[x,"case"],"_",TP[x,"time"])
}

HP<-read.csv("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_HP_ITER01_RunSims_Output_06.30.21_JP.csv",header=TRUE)
HP$drug<-"HP"
for (x in 1:nrow(HP)) {
  HP[x,"case_time"]<-paste0(HP[x,"case"],"_",HP[x,"time"])
}

#TH<-read.csv("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_THP_ITER01_RunSims_Output_06.30.21_JP.csv",header=TRUE)
#TH$drug<-"TH"
#for (x in 1:nrow(TH)) {
#  TH[x,"case_time"]<-paste0(TH[x,"case"],"_",TH[x,"time"])
#}

THP<-read.csv("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_THP_ITER01_RunSims_Output_06.30.21_JP.csv",header=TRUE)
THP$drug<-"THP"
for (x in 1:nrow(THP)) {
  THP[x,"case_time"]<-paste0(THP[x,"case"],"_",THP[x,"time"])
}

### merge dfs together
foo<-merge(TP,HP,by="case_time")
ALL<-merge(foo,THP,by="case_time"); rm(foo); length(unique(ALL$case))

setwd("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_SimResults/")

### loop that creates 2 plots per patient (vol/LD)
for (x in names(table(ALL$case))) {
  caseID<-x
  
  drug00<-ALL[which(ALL$case == caseID),c("case.x","time.x","volume.x","ld.x","drug.x")]
  colnames(drug00)<-c("case","time","vol","ld","drug")
  
  drug01<-ALL[which(ALL$case == caseID),c("case.y","time.y","volume.y","ld.y","drug.y")]
  colnames(drug01)<-colnames(drug00)
  
  drug02<-ALL[which(ALL$case == caseID),c("case","time","volume","ld","drug")]
  colnames(drug02)<-colnames(drug00)
  
  #drug03<-ALL[which(ALL$case == caseID),c("case.3","time.3","vol.3","ld.3","drug.3")]
  #colnames(drug03)<-colnames(drug00)
  
  df<-rbind(drug00,drug01,drug02)
  
  par(mfrow=c(1,2))
  dodge <- position_dodge(width=0.1) 
  
  p1<-ggplot(data=df, aes(x=time, y=vol, group=drug)) +
    geom_line(aes(linetype=drug),size=1)+
    geom_point(aes(color=drug),position=dodge, size=3) +
    ggtitle(paste0(caseID,": predicted tumor volume across study arms (regimens)")) +
    xlab("Treatment time (weeks)") + ylab("Tumor volume (cc)") + 
    theme(plot.title = element_text(hjust = 0.5))

  p2<-ggplot(data=df, aes(x=time, y=ld, group=drug)) +
    geom_line(aes(linetype=drug),size=1)+
    geom_point(aes(color=drug),position=dodge, size=3) +
    ggtitle(paste0(caseID,": predicted tumor longest dimension across study arms (regimens)")) +
    xlab("Treatment time (weeks)") + ylab("Tumor longest dimension (cm)") + 
    theme(plot.title = element_text(hjust = 0.5))

  p3<-grid.arrange(p1, p2, nrow = 1)
  
  ggsave(p3, filename = paste0("SBS_NEO_",caseID,"_VOL_LD_06.30.21_JP.pdf"))
  
  dev.off()
  
}

### combine all the plots in the dir and save as new PDF
pdf_combine(list.files(), output = "SBS_NEO_ALL_VOL_LD_06.30.21_JP.pdf")

###############################################################################################################
###############################################################################################################
####################      
####################      MODULE #2: TABULATING PCR (OVERALL/STRATIFIED)
####################      
###############################################################################################################
###############################################################################################################

### TP PCR
setwd("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_TP/")

TP_PCR<-NULL; START_VOL<-NULL; END_VOL<-NULL; PERC_CHANGE<-NULL; SIM<-NULL
for (file in list.files("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_TP/")) {
  sim<-sub(".json", "",file)
  SIM<-append(SIM, sim)
  json<-fromJSON(file = file)
  startvol<-round(json$Longitudinal$`cancer volume`[1],2)
  START_VOL<-append(START_VOL,startvol)
  endvol<-round(tail(json$Longitudinal$`cancer volume`, n=1),2)
  END_VOL<-append(END_VOL,endvol)
  perc_remain<-endvol/startvol
  perc_change<-round((1-perc_remain)*100,3)
  PERC_CHANGE<-append(PERC_CHANGE, perc_change)
  if(endvol < 0.01 || perc_change >= 99.9) {
    pcr<-TRUE
  }  else {
    pcr<-FALSE
  }
  TP_PCR<-append(TP_PCR, pcr)
}

TP_PCR
length(TP_PCR)
table(TP_PCR)

TP_RESULTS<-cbind(SIM, START_VOL, END_VOL, PERC_CHANGE, TP_PCR); TP_RESULTS





### TH PCR
setwd("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_TH/")

TH_PCR=NULL
for (file in list.files("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_TH/")) {
  json<-fromJSON(file = file)
  pcr<-tail(fromJSON(file = file)$Longitudinal$`cancer volume`,n=1)<0.01
  TH_PCR<-append(TH_PCR, pcr)
}
TH_PCR
length(TH_PCR)
table(TH_PCR)

### THP PCR
setwd("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_THP/")

THP_PCR=NULL
for (file in list.files("/home/johnpfeiffer/Desktop/JUPY/sbs_clinicaltrialdocs/VirtualTrials/Trial_NEO/NEO_RunSims/SBS_NEO_TH/")) {
  pcr<-tail(fromJSON(file = file)$Longitudinal$`cancer volume`,n=1)<0.1
  THP_PCR<-append(THP_PCR, pcr)
}
THP_PCR
length(THP_PCR)
table(THP_PCR)

























