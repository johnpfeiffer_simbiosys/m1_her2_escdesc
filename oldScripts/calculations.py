import numpy as np
def calculate_tumor_features():
    for stat in tf_stats:
        for meas in tumor_features:
            attribute_grabbed = getattr(s, attribute_dict[meas])
            active_vec = attribute_grabbed.VolFraction[s.times[y]]
            case_dict[stat + '_' + meas + '_' + dict_week] = fxn_dict[stat](active_vec)

def calculate_growth(stats, data, index, functions, dict_wk, output, phases):
    for stat in stats:
        active_vec = data.cancer.GrowthRate[data.times[index]][(data.cancer.VolFraction[data.times[index]] > 0)]
        output[stat + '_' + 'growth' + '_' + dict_wk] = functions[stat](active_vec)

    total_growing = np.sum(data.cancer.GrowthRate[data.times[index]][data.cancer.VolFraction[data.times[index]] > 0] > 0)
    output['total_growing_' + dict_wk] = total_growing
    total_nongrowing = np.sum(data.cancer.GrowthRate[data.times[index]][data.cancer.VolFraction[data.times[index]] > 0] == 0)
    output['total_nongrowing_' + dict_wk] = total_nongrowing
    total_dying = np.sum(data.cancer.GrowthRate[data.times[index]][data.cancer.VolFraction[data.times[index]] > 0] < 0)
    output['total_dying_' + dict_wk] = total_dying
    
    for gp in phases:
        fraction_growing = output['total' + '_' + gp + '_' + dict_wk]/(total_growing + total_nongrowing + total_dying)
        output['fraction' + '_' + gp + '_' + dict_wk] = fraction_growing

def calculate_metabolic():
    cvf = s.cancer.Volume[s.times[y]]
    for nutr in nutrients:
        active_nutr = getattr(s, nutr)
        active_vector = active_nutr[s.times[y]]
        thresh = np.max(active_vector)/1e6
        lesser = cvf[active_vector < thresh].sum()
        greater = cvf[active_vector > thresh].sum()
        low_nutr_frac = lesser/(lesser+greater)*100
        case_dict['low' + '_' + nutr + '_' + 'frac' + '_' + dict_week] = low_nutr_frac
        active_flux = getattr(s.cancer, nutr)
        active_vector = active_flux[s.times[y]]
        thresh = nutrient_thresh[nutr]
        lesser = cvf[active_vector > thresh].sum()
        greater = cvf[active_vector < thresh].sum()
        low_nutr_influx_frac = lesser/(lesser+greater)*100
        case_dict['low' + '_' + nutr + '_' + 'influx_frac' + '_' + dict_week] = low_nutr_influx_frac
        
def calculate_microvasculature():
    mask = s.cancer.VolFraction[s.times[y]]
    
    #K
    k = s.K[s.times[y]]
    for stat in mv_stats: 
        case_dict[stat + '_' + 'k' + '_' + dict_week] = fxn_dict[stat](k[mask > 0])
    
    #Phi
    phi = s.Phi[s.times[y]]
    for stat in mv_stats: 
        case_dict[stat + '_' + 'phi' + '_' + dict_week] = fxn_dict[stat](phi[mask > 0])
    
    #kt
    kt = k*phi
    for stat in mv_stats: 
        case_dict[stat + '_' + 'kt' + '_' + dict_week] = fxn_dict[stat](kt[mask > 0])
    
    #kep
    kep = (k*phi)/(1-phi)
    for stat in mv_stats: 
        case_dict[stat + '_' + 'kep' + '_' + dict_week] = fxn_dict[stat](kep[mask > 0])