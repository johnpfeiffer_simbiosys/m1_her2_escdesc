##################################
###### READ IN DEPENDENCIES

library(gdata)

##################################
##################################

##################################
###### GET COHORTS FROM ALLDATA

clin<-read.csv("/Users/johnpfeiffer/Desktop/m1_her2_escdesc/input/SBS_AllData_DataEntry_02.24.22_JP.csv")

### select only her2 positive patients
status<-which(clin$fdaHER2 == "HER2+")
ihc_01<-which(clin$receptorHER2 == "3+")
ihc_02<-which(clin$receptorHER2 == "+")
fish<-which(as.numeric(clin$receptorHER2ISHRatio)>2.0)
clin_her2<-clin[unique(c(status, ihc_01, ihc_02, fish)),]
rownames(clin_her2)<-seq(from = 1, to = nrow(clin_her2))

### seperate pcr patients from residual
### residual will be escalation candidates
### pCR will be de-escalation candidates
clin_her2_pcr<-clin_her2[which(clin_her2$pathCompleteResponse == "pCR"),]
clin_her2_resid<-clin_her2[which(clin_her2$pathCompleteResponse == "Residual"),]

### clean env
keep(clin_her2_pcr, clin_her2_resid, clin, sure=TRUE); gc()

##################################
##################################

##################################
###### SELECT CANDIDATES FROM RESIDUAL/ESCALATION GROUP BASED ON REGIMEN

table(clin_her2_resid$actualRegimen)

# First, we subset our virtual tumor bank to include participants with HER2+ disease. We then captured a further subset, whose participants did not achieve pCR upon completion of their neoadjuvent treatment.
# Finally, we considered patients who received a regimen other than TCHP. This left us with 206 - 96 = 110 patients in the final cohort eligible for escalation. 
# Included any participant with a regimen that included at least one of: carboplatin, herceptin (trastuzumab), perjeta (pertuzumab).

keep_esc_reg<-c("AC-3TH",
"AC-3TH or AC-3T",
"AC-TH+Lapatinib",
"AC-wTH",
"ddAC-TH",
"ddAC-TH or ddAC-T",
"ddAC-wTHP",
"H",
"HL",
"TC",
"TCarbo",
"TCH",
"TCH-ddAC",
"TH",
"THP",
"THP-ddAC",
"wHL-TH",
"wTH-ddAC",
"wTH(AMG386)-ddAC",
"wTH(MK2206)-ddAC",
"wTHP-ddAC",
"wTP(T-DM1)-ddAC")

clin_her2_resid<-clin_her2_resid[clin_her2_resid$actualRegimen %in% keep_esc_reg,]

### create most of the runSims sheet
clin_her2_resid$caseID<-clin_her2_resid$caseID_TS
clin_her2_resid$depictionID<-NA
clin_her2_resid$useMV<-1
clin_her2_resid$lagTime<-3*24*60*60 # in seconds
clin_her2_resid$endTime<-22*7*24*60*60
clin_her2_resid$regimen<-"TCHP"
clin_her2_resid$mvMap<-NA
clin_her2_resid$phenotyper<-"TreeNGF"

runSims<-clin_her2_resid[,(ncol(clin_her2_resid)-7):ncol(clin_her2_resid)]
write.csv(runSims, "/home/johnpfeiffer/Venv/m1_her2_escdesc/runSims/SBS_HER2_Escalators_RunSims_03.14.22_JP.csv", row.names = FALSE)

### what follows was done in command line: for each case, verified depictions were identified. where no verified depiction existed, we chose the most recent depiction. 
### one case was eliminated due to missing DCE/depiction. we verified the quality of the previously unverified depictions. we then assessed their mv maps. 
### within each case, we compared the mv4 and mv6 map and chose the best fit map. if neither map met the quality threshold, the case was excluded from the cohort. 
### after DCE, tumorSeg, multiSeg, and mv map quality control, we launched simulation for the remaining ___ cases. 

